# Clockify-cli

## Installation

    wget https://bitbucket.org/djanlooka/clockify-cli/raw/5621803ec349054fef35eb9b8627f50af9e7e18b/dist/clockify_cli-0.0.1-py3-none-any.whl
    pip3 install clockify_cli-0.0.1-py3-none-any.whl
    
(you need python 3 and pip3. For debian: `sudo apt-get install python3-pip`).


## Use

Start with

    clockify configure
    
and follow the instructions to set up the authentication.
You might then continue with 

    clockify check
   
to have an overview of the days which are still missing entries.
Once you decided on an entry to add to a certain day, type

    clockify tasks
    
For a complete list of task names. You will need to know the name
of a task for the next command. For example,

    clockify add --date 2018-11-25 --hours 8 --task '%t3.2%' --description Meetings

will add an entry for task `WP3 - Demonstration, Client Implementation & Integration / T3.2 - Demonstration`
There are also shortcuts and defaults, like:

    clockify add --date 25 --task '%t3.2%' --description Meetings

to add an entry of 8 hours (default duration) to day 25 of current month. 