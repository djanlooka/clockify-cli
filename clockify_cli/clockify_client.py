import requests
import dateutil.parser
import datetime
import os.path
import pickle
from pytz import timezone
from clockify_cli import utils
from clockify_cli.api import Task, DecoratedTask, Entry, DecoratedEntry, TimeSlot, DayEntries

BASE_URL = 'https://api.clockify.me/api'


class BasicClockifyClient:
    """
    Exposes a method for each endpoint of the Clockify API.

    See: https://clockify.github.io/clockify_api_docs/
    """

    api_token = ''

    def __init__(self, api_token):
        self.api_token = api_token

    def get_workspaces(self):
        """
        :return: a list of (id, name) tuples
        """
        json = self._get_with_token_(BASE_URL + '/workspaces/')
        return [(w['id'], w['name']) for w in json]

    def get_users(self, workspace_id):
        """
        :return: a list of (id, email) tuples
        """
        json = self._get_with_token_('{}/workspaces/{}/users/'.format(BASE_URL, workspace_id))
        return [(u['id'], u['email']) for u in json]

    def get_entries(self, workspace_id, user_id, page=0):
        """
        :return: a list of Entry objects
        """
        json = self._get_with_token_(
            '{}/workspaces/{}/timeEntries/user/{}/?page={}'.format(BASE_URL, workspace_id, user_id, page))
        return [
            Entry(
                utils.utc_date_to_dutch(dateutil.parser.parse(e['timeInterval']['start'])),
                utils.utc_date_to_dutch(dateutil.parser.parse(e['timeInterval']['end'])),
                e['projectId'],
                e['taskId'],
                e['description'])
            for e in json['timeEntriesList']]

    def get_projects(self, workspace_id):
        """
        :return: a list of (id, name) tuples
        """
        json = self._get_with_token_('{}/workspaces/{}/projects/'.format(BASE_URL, workspace_id))
        return [(p['id'], p['name']) for p in json]

    def get_tasks(self, workspace_id, project_id):
        """
        :return: a list of Task objects.
        """
        json = self._get_with_token_(
            '{}/workspaces/{}/projects/{}/tasks/'.format(BASE_URL, workspace_id, project_id))
        for t in json:
            yield Task(project_id, t['id'], t['name'])

    def _add_entry_(self, workspace_id, date, slot, project_id, task_id, description):
        start = datetime.datetime(date.year, date.month, date.day, slot.start.hour, 0, 0)
        start = utils.date_with_dutch_timezone(start).astimezone(timezone('UTC'))
        end = datetime.datetime(date.year, date.month, date.day, slot.end.hour, 0, 0)
        end = utils.date_with_dutch_timezone(end).astimezone(timezone('UTC'))
        payload = {
            'start': start.isoformat().replace('+00:00', 'Z'),
            'end': end.isoformat().replace('+00:00', 'Z'),
            'description': description,
            'tagIds': [],
            'billable': 'true',
            'taskId': task_id,
            'projectId': project_id
        }
        self._post_with_token_('{}/workspaces/{}/timeEntries/'.format(BASE_URL, workspace_id), payload)

    def _get_with_token_(self, url):
        """
        Performs an HTTP GET taking care of authentication
        """
        response = requests.get(url, headers={'X-Api-Key': self.api_token, 'content-type': 'application/json'})
        if response.status_code != 200:
            raise Exception('Received status code {}.'.format(response.status_code))
        return response.json()

    def _post_with_token_(self, url, payload):
        """
        Performs an HTTP POST taking care of authentication
        """
        response = requests.post(url,
                                 headers={'X-Api-Key': self.api_token, 'content-type': 'application/json'},
                                 json=payload)
        if response.status_code != 201:
            raise Exception('Received status {} / {}.'.format(response.status_code, response.text))
        return response.json()


class ClockifyClient(BasicClockifyClient):
    """
    Builds on BasicClockifyClient to give higher level access to the API.
    """

    workspace_id = None
    my_user_id = None
    task_cache = None

    def __init__(self, api_token, workspace_name, user_email, config_path):
        super(ClockifyClient, self).__init__(api_token)
        self.workspace_id = self._get_workspace_id_(workspace_name)
        self.my_user_id = self._get_user_id_by_email_(self.workspace_id, user_email)
        self.task_cache = TaskCache(self, config_path, self.workspace_id)

    def _get_workspace_id_(self, workspace_name):
        workspaces = self.get_workspaces()
        for id, name in workspaces:
            if name == workspace_name:
                return id
        raise Exception('No {} workspace found.'.format(workspace_name))

    def _get_user_id_by_email_(self, workspace_id, user_email):
        users = self.get_users(workspace_id)
        for id, email in users:
            if email == user_email:
                return id
        raise Exception('No user found with email {}.'.format(user_email))

    def get_decorated_entries(self, workspace_id, user_id, page=0):
        entries = self.get_entries(workspace_id, user_id, page)
        return [DecoratedEntry(entry,
                               self.task_cache.project_name(entry.project_id),
                               self.task_cache.task(entry.task_id).task_name)
                for entry in entries]

    def add_entry(self, date, slot, project_id, task_id, description):
        self._add_entry_(self.workspace_id, date, slot, project_id, task_id, description)

    def get_entries_per_day(self):
        return DayEntriesIterable(EntryIterable(self, self.workspace_id, self.my_user_id))

    def find_tasks_by_pattern(self, pattern):
        return self.task_cache.find_tasks_by_pattern(pattern)

    def project_name(self, project_id):
        return self.task_cache.project_name(project_id)

    def task(self, task_id):
        return self.task_cache.task(task_id)

    def get_all_tasks(self):
        return self.task_cache.get_all_tasks()


class TaskCache:
    project_by_id = {}
    task_by_id = {}

    def __init__(self, clockify_client, config_directory, workspace_id):
        cache_path = config_directory + '/cache.p'
        if os.path.exists(cache_path):
            self.project_by_id, self.task_by_id = pickle.load(open(cache_path, "rb"))
        else:
            for project_id, name in clockify_client.get_projects(workspace_id):
                self.project_by_id[project_id] = name
            for project_id in self.project_by_id.keys():
                for task in clockify_client.get_tasks(workspace_id, project_id):
                    self.task_by_id[task.task_id] = DecoratedTask(task, self.project_by_id[project_id])
            pickle.dump((self.project_by_id, self.task_by_id), open(cache_path, "wb"))

    def project_name(self, project_id):
        return self.project_by_id[project_id] if project_id in self.project_by_id else 'UNKNOWN ' + project_id

    def task(self, task_id):
        return self.task_by_id[task_id]

    def find_tasks_by_pattern(self, pattern):
        pattern = pattern.lower()
        tasks = []
        for task_id in self.task_by_id:
            task = self.task_by_id[task_id]
            if pattern.startswith('%') and pattern.endswith('%'):
                if pattern.replace('%', '') in task.task_name.lower():
                    tasks.append(task)
            elif pattern.startswith('%'):
                if task.task_name.lower().endswith(pattern.replace('%', '')):
                    tasks.append(task)
            elif pattern.endswith('%'):
                if task.task_name.lower().startswith(pattern.replace('%', '')):
                    tasks.append(task)
            else:
                if task.task_name.lower() == pattern.lower():
                    tasks.append(task)
        return tasks

    def find_task_by_name_x(self, task_name):
        for task_id in self.task_by_id:
            task = self.task_by_id[task_id]
            if task_name.lower() == task.task_name.lower():
                return task
        return None

    def get_all_tasks(self):
        tasks = [v for v in self.task_by_id.values()]
        tasks.sort(key=lambda t: t.project_name + t.task_name)
        return tasks


class EntryIterable:
    clockify_client = None
    workspace_id = None
    my_user_id = None
    page = None
    page_idx = 0
    entry_idx = 0
    end_reached = False

    def __init__(self, clockify_client, workspace_id, my_user_id):
        self.clockify_client = clockify_client
        self.workspace_id = workspace_id
        self.my_user_id = my_user_id

    def __iter__(self):
        return self

    def __next__(self):
        if self.end_reached:
            raise StopIteration()
        if self.page is None:
            self.page = self.clockify_client.get_decorated_entries(self.workspace_id, self.my_user_id, page=self.page_idx)
            if len(self.page) == 0:
                self.end_reached = True
                raise StopIteration()
        elif self.entry_idx >= len(self.page):
            self.page_idx = self.page_idx + 1
            self.page = self.clockify_client.get_decorated_entries(self.workspace_id, self.my_user_id, page=self.page_idx)
            if len(self.page) == 0:
                self.end_reached = True
                raise StopIteration()
            self.entry_idx = 0
        entry = self.page[self.entry_idx]
        self.entry_idx = self.entry_idx + 1
        return entry


class DayEntriesIterable:
    entry_iterable = None
    last_entry = None

    def __init__(self, entry_iterable):
        self.entry_iterable = entry_iterable

    def __iter__(self):
        return self

    def __next__(self):
        date = None
        entries = []
        if self.last_entry is not None:
            date = self.last_entry.date()
            entries.append(self.last_entry)
            self.last_entry = None
        try:
            for entry in self.entry_iterable:
                if date is None:
                    date = entry.date()
                    entries.append(entry)
                elif entry.date() == date:
                    entries.append(entry)
                else:
                    self.last_entry = entry
                    break
        except StopIteration:
            pass
        if date is not None:
            return DayEntries(entries)
        raise StopIteration
