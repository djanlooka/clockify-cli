from builtins import FileNotFoundError

from clockify_cli.clockify_client import ClockifyClient
import argparse
from pathlib import Path
from clockify_cli import commands
import json
from clockify_cli.credentials import Credentials
from clockify_cli.utils import CONFIG_DIR
import sys

WORKSPACE_NAME = 'MD - Innovatiekrediet'


def load_credentials(config_directory):
    api_token = None
    user_email = None
    try:
        with open(config_directory + '/config.json') as f:
            config = json.load(f)
            if 'api_token' not in config:
                raise Exception('api_token missing from ~/.clockify-cli/config.json file.')
            api_token = config['api_token']
            if 'user_email' not in config:
                raise Exception('user_email missing from ~/.clockify-cli/config.json file.')
            user_email = config['user_email']
    except (FileNotFoundError, NotADirectoryError):
        raise Exception('API_TOKEN or USER_EMAIL missing from ~/.clockify-cli/config.json file.')
    if api_token is None or user_email is None:
        raise Exception('API_TOKEN or USER_EMAIL missing from ~/.clockify-cli/config.json file.')
    return api_token, user_email


class ClockifyCLI:

    def main(self):
        if len(sys.argv) == 2 and sys.argv[1] == 'configure':
            commands.ConfigureCommand(WORKSPACE_NAME).execute(None)
            return
        elif len(sys.argv) == 2 and sys.argv[1] == 'clearcache':
            commands.ClearCacheCommand().execute(None)
            return
        try:
            credentials = Credentials(CONFIG_DIR)
            api_key, user_email = credentials.load_credentials()
        except Exception as err:
            raise Exception('ERROR: {}\n\ntry running "clockify configure" to set your credentials'.format(err))
        config_path = '{}/{}'.format(str(Path.home()), CONFIG_DIR)
        client = ClockifyClient(api_key, WORKSPACE_NAME, user_email, config_path)
        parser = argparse.ArgumentParser(description='CLI tool to manage your Clockify entries',
                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        subparsers = parser.add_subparsers()

        commands.EntriesCommand(client).configure_parser(subparsers)
        commands.AddCommand(client).configure_parser(subparsers)
        commands.CheckCommand(client).configure_parser(subparsers)
        commands.CopyCommand(client).configure_parser(subparsers)
        commands.TasksCommand(client).configure_parser(subparsers)
        args = parser.parse_args()
        if not hasattr(args, 'command'):
            parser.print_help()
            return
        args.command.execute(args)


def entry_point():
    try:
        ClockifyCLI().main()
    except Exception as err:
        print('ERROR: {}'.format(err))
        raise err


if __name__ == '__main__':
    entry_point()

