from dateutil import parser

from clockify_cli.clockify_client import TimeSlot
from clockify_cli.clockify_client import ClockifyClient
from clockify_cli import utils
import argparse
import datetime
from clockify_cli.credentials import Credentials
import re
from pathlib import Path
import os

#TODO: configure command

#TODO: clear cache command

class CliCommand:
    clockify_client = None

    def __init__(self, clockify_client):
        self.clockify_client = clockify_client

    def execute(self, cliargs):
        pass


#TODO: dates as -1
class EntriesCommand(CliCommand):

    def __init__(self, clockify_client):
        super(EntriesCommand, self).__init__(clockify_client)

    def configure_parser(self, subparsers):
        parser_entries = subparsers.add_parser('entries', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser_entries.add_argument('-n', '--number', type=int, default=10, help='Max. number of entries to display.')
        parser_entries.add_argument('-x', '--extended', action='store_true', help='Extended output.')
        parser_entries.set_defaults(command=self)

    def execute(self, cliargs):
        count = cliargs.number
        if cliargs.extended:
            self.render_extended(count)
        else:
            self.render_compact(count)

    def render_compact(self, count):
        current_date = None
        for day_entries in self.clockify_client.get_entries_per_day():
            if current_date and utils.is_across_weekend(day_entries.date(), current_date):
                print('--- WEEKEND ---')
            longest_entry = None
            for entry in day_entries.entries:
                if longest_entry:
                    if entry.duration() > longest_entry.duration():
                        longest_entry = entry
                else:
                    longest_entry = entry
            if len(day_entries.entries) > 1:
                print('{}    {} hours total    "{}" and more'.format(
                    day_entries.date().isoformat(),
                    utils.delta_to_hours(day_entries.total_time()),
                    longest_entry.task_name))
            else:
                print('{}    {} hours total    "{}"'.format(
                    day_entries.date().isoformat(),
                    utils.delta_to_hours(day_entries.total_time()),
                    longest_entry.task_name))
            count = count - 1
            if count == 0:
                break
            current_date = day_entries.date()

    def render_extended(self, count):
        for day_entries in self.clockify_client.get_entries_per_day():
            print('\n{} - {} hours total'.format(
                day_entries.date().isoformat(),
                utils.delta_to_hours(day_entries.total_time())))
            for entry in day_entries.entries:
                hours = utils.delta_to_hours(entry.end - entry.start)
                print('  {0}h  {1: <30} {2: <50} "{3}"'.format(hours, entry.project_name, entry.task_name, entry.description))
            count = count - 1
            if count == 0:
                break


class CheckCommand(CliCommand):

    def __init__(self, clockify_client):
        super(CheckCommand, self).__init__(clockify_client)

    def configure_parser(self, subparsers):
        parser_entries = subparsers.add_parser('check', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser_entries.add_argument('-n', '--number', type=int, default=30, help='Number of days to check.')
        parser_entries.set_defaults(command=self)

    def execute(self, cliargs):
        number = cliargs.number
        to_date = datetime.datetime.now().date() + datetime.timedelta(-1)
        from_date = to_date + datetime.timedelta(-number)
        day_entries_list = self._get_day_entries(from_date, to_date)
        days_with_entries = set()
        for day_entries in day_entries_list:
            days_with_entries.add(day_entries.date())
        days_without_entries = []
        for date in (to_date + datetime.timedelta(-n) for n in range(number)):
            if not utils.is_weekend(date) and date not in days_with_entries:
                days_without_entries.append(date)
        underworked_days = []
        overworked_days = []
        for day_entries in day_entries_list:
            if utils.delta_to_hours(day_entries.total_time()) < 8:
                underworked_days.append((day_entries.date(), utils.delta_to_hours(day_entries.total_time())))
            elif utils.delta_to_hours(day_entries.total_time()) > 8:
                overworked_days.append((day_entries.date(), utils.delta_to_hours(day_entries.total_time())))
        if days_without_entries:
            print('\nDAYS WITHOUT ANY ENTRY:')
            for date in days_without_entries:
                print('    {0}'.format(date.isoformat()))
        if underworked_days:
            print('\nDAYS WITH LESS THAN 8 HOURS:')
            for date, hours in underworked_days:
                print('    {0} {1: >2}h'.format(date.isoformat(), hours))
        if overworked_days:
            print('\nDAYS WITH MORE THAN 8 HOURS:')
            for date, hours in overworked_days:
                print('    {0} {1: >2}h'.format(date.isoformat(), hours))

    def _get_day_entries(self, from_date, to_date):
        day_entries_list = []
        for day_entries in self.clockify_client.get_entries_per_day():
            if day_entries.date() < from_date:
                return day_entries_list
            if day_entries.date() <= to_date:
                day_entries_list.append(day_entries)
        return day_entries_list


class TasksCommand(CliCommand):

    def __init__(self, clockify_client):
        super(TasksCommand, self).__init__(clockify_client)

    def configure_parser(self, subparsers):
        parser_add = subparsers.add_parser('tasks', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser_add.set_defaults(command=self)

    def execute(self, cliargs):
        print('{0: <60} {1: <40} '.format('PROJECT', 'TASK'))
        print('-----------------------------------------------------------------')
        for task in self.clockify_client.get_all_tasks():
            print('{0: <60} {1: <40} '.format(task.project_name, task.task_name))


class AddCommand(CliCommand):

    def __init__(self, clockify_client):
        super(AddCommand, self).__init__(clockify_client)

    def configure_parser(self, subparsers):
        parser_add = subparsers.add_parser('add', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser_add.add_argument('-d', '--date', type=str, default='"today"',
                                help='''Date specified as yyyy-mm-dd (complete date); mm-dd 
                                (month and day in current year); dd (day in current month and year);
                                "today".''')
        parser_add.add_argument('-o', '--hours', type=int, default=8, help='Number of hours to entry.')
        parser_add.add_argument('-t', '--task', required=True, type=str,
                                help='The name of the task. You can use "%%" '
                                     'to specify part of the name, similarly to SQL')
        parser_add.add_argument('-e', '--description', required=False, type=str,
                                help='Optional description associated to the entry.')
        parser_add.set_defaults(command=self)

    def execute(self, cliargs):
        date = utils.parse_date_argument(cliargs.date)
        day_entries = self._find_day_entry_(date)
        if day_entries:
            slots = day_entries.find_slots(cliargs.hours)
        else:
            slots = [TimeSlot(9, 9 + cliargs.hours)]
        if not slots:
            print('No available slots found on date {}'.format(date))
            return
        tasks = self.clockify_client.find_tasks_by_pattern(cliargs.task)
        if len(tasks) == 0:
            print('No task found matching pattern "{}"'.format(cliargs.task))
            return
        elif len(tasks) > 1:
            print('Multiple tasks found matching pattern "{}":'.format(cliargs.task))
            for task in tasks:
                print('  {} / {}'.format(self.clockify_client.project_name(task.project_id), task.task_name))
            return
        task = tasks[0]
        for slot in slots:
            print('Adding new entry on date {} with following data:'.format(date))
            print('       Slot: {}'.format(slot))
            print('    Project: {}'.format(self.clockify_client.project_name(task.project_id)))
            print('       Task: {}'.format(task.task_name))
            print('Description: {}'.format(cliargs.description))
            self.clockify_client.add_entry(date, slot, task.project_id, task.task_id, cliargs.description)
            print('Done.')

    def _find_day_entry_(self, date):
        iterable = self.clockify_client.get_entries_per_day()
        while True:
            try:
                day_entries = next(iterable)
                if day_entries.date() == date:
                    return day_entries
                elif day_entries.date() < date:
                    return None
            except StopIteration:
                return None


class CopyCommand(CliCommand):

    def __init__(self, clockify_client):
        super(CopyCommand, self).__init__(clockify_client)

    def configure_parser(self, subparsers):
        parser_add = subparsers.add_parser('copy', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser_add.add_argument('-f', '--fromdate', required=True, type=str,
                                help='Date from which to copy entries, in yyyy-mm-dd format.')
        parser_add.add_argument('-t', '--todate', required=True, type=str,
                                help='Date to which to copy entries, in yyyy-mm-dd format.')
        parser_add.set_defaults(command=self)

    def execute(self, cliargs):
        fromdate = utils.parse_date_argument(cliargs.fromdate)
        todate = utils.parse_date_argument(cliargs.todate)
        days_entries_in_to_date = self._get_day_entries(todate, todate)
        if days_entries_in_to_date:
            print("There are already {} entries in the destination date {}. Aborting copy.".format(
                len(days_entries_in_to_date),
                todate))
            return
        days_entries_in_from_date = self._get_day_entries(fromdate, fromdate)
        if not days_entries_in_from_date:
            print('There are no entries in the source date {}. Nothing to copy.'.format(fromdate))
            return
        if len(days_entries_in_from_date) > 1:
            print('This is not supposed to happen, there is probably a bug: '
                  'search of entries for date {} returned entries in {} dates'.format(
                    fromdate,
                    len(days_entries_in_from_date)))
        for entry in days_entries_in_from_date[0].entries:
            slot = entry.slot()
            print('Adding new entry on date {} with following data:'.format(todate))
            print('       Slot: {}'.format(slot))
            print('    Project: {}'.format(self.clockify_client.project_name(entry.project_id)))
            print('       Task: {}'.format(self.clockify_client.task(entry.task_id).task_name))
            print('Description: {}'.format(entry.description))
            self.clockify_client.add_entry(todate, slot, entry.project_id, entry.task_id, entry.description)

    def _get_day_entries(self, from_date, to_date):
        """
        TODO: move this to superclass
        """
        day_entries_list = []
        for day_entries in self.clockify_client.get_entries_per_day():
            if day_entries.date() < from_date:
                return day_entries_list
            if day_entries.date() <= to_date:
                day_entries_list.append(day_entries)
        return day_entries_list


class ConfigureCommand:
    workspace_name = None

    def __init__(self, workspace_name):
        self.workspace_name = workspace_name

    def execute(self, cliargs):
        api_key = input('Enter your API key (you find it on https://clockify.me/user/settings): ')
        if not re.match('[a-zA-Z0-9\/]+', api_key):
            raise Exception('{} is not a valid API token'.format(api_key))
        user_email = input('Enter your email address: ')
        if not re.match('[^@]+@[^@]+', user_email):
            raise Exception('{} is not a valid email address'.format(api_key))
        credentials = Credentials(utils.CONFIG_DIR)
        credentials.save_credentials(api_key, user_email)
        print('Credentials saved. Testing...')
        api_key, user_email = credentials.load_credentials()
        config_path = '{}/{}'.format(str(Path.home()), utils.CONFIG_DIR)
        client = ClockifyClient(api_key, self.workspace_name, user_email, config_path)
        print('Test successful. User ID: {}'.format(client.my_user_id))


class ClearCacheCommand:

    def execute(self, cliargs):
        config_path = '{}/{}'.format(str(Path.home()), utils.CONFIG_DIR)
        cache_path = config_path + '/cache.p'
        if os.path.exists(cache_path):
            os.remove(cache_path)
            print('Cache is cleared.')
        else:
            print('No cache to clear.')



