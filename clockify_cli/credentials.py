from builtins import FileNotFoundError

import json
import re
import os
from pathlib import Path

# TODO: finish
class Credentials:
    config_directory = None
    config_path = None

    def __init__(self, config_directory):
        self.config_directory = config_directory
        self.config_path = '{}/{}'.format(str(Path.home()), self.config_directory)

    def load_credentials(self):
        api_token = None
        user_email = None
        try:
            with open(self.config_path + '/config.json') as f:
                config = json.load(f)
                if 'api_token' not in config:
                    raise Exception('api_token missing from {}/config.json file.'.format(self.config_path))
                api_token = config['api_token']
                if 'user_email' not in config:
                    raise Exception('user_email missing from {}/config.json file.'.format(self.config_path))
                user_email = config['user_email']
        except (FileNotFoundError, NotADirectoryError):
            raise Exception('API_TOKEN or USER_EMAIL missing from ~/.clockify-cli/config.json file.')
        if api_token is None or user_email is None:
            raise Exception('API_TOKEN or USER_EMAIL missing from ~/.clockify-cli/config.json file.')
        return api_token, user_email

    def save_credentials(self, api_token, user_email):
        if not re.match('[a-zA-Z0-9\/]+', api_token):
            raise Exception('{} is not a valid API token'.format(api_token))
        if not re.match('[^@]+@[^@]+', user_email):
            raise Exception('{} is not a valid email address'.format(api_token))
        if not os.path.exists(self.config_path):
            print('Creating configuration directory {}'.format(self.config_path))
            os.makedirs(self.config_path)
        with open(self.config_path + '/config.json', 'w') as f:
            json.dump({'api_token': api_token, 'user_email': user_email}, f)



