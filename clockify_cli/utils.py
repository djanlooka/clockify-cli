import dateutil.parser
from pytz import timezone
import datetime
import dateutil
import re

BASE_URL = 'https://api.clockify.me/api'
DUTCH_TIMEZONE = 'Europe/Amsterdam'
CONFIG_DIR='.clockify-cli'


def utc_date_to_dutch(dt):
    return dt.astimezone(timezone(DUTCH_TIMEZONE))


def date_with_dutch_timezone(naive_date):
    return timezone(DUTCH_TIMEZONE).localize(naive_date)


def delta_to_hours(delta):
    return int(delta.seconds / 3600)


def is_across_weekend(date1, date2):
    return date1.weekday() >= date2.weekday() or (date2 - date1).days >=7


def is_weekend(date):
    return date.weekday() == 5 or date.weekday() == 6


def parse_date_argument(datestr):
    if re.match('\\d\\d\\d\\d-\\d\\d-\\d\\d', datestr):
        return dateutil.parser.parse(datestr).date()
    elif re.match('\\d\\d-\\d\\d', datestr):
        current_year = datetime.datetime.now().year
        return dateutil.parser.parse('{}-{}'.format(current_year, datestr)).date()
    elif re.match('\\d\\d', datestr):
        current_year = datetime.datetime.now().year
        current_month = datetime.datetime.now().month
        return dateutil.parser.parse('{}-{}-{}'.format(current_year, current_month, datestr)).date()
    elif datestr == 'today':
        return datetime.datetime.now().date()
    raise Exception('Unknown date format {}'.format(datestr))

