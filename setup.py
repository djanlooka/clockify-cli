import setuptools

REQUIREMENTS = [i.strip() for i in open('requirements.txt').readlines()]

setuptools.setup(
    name='clockify-cli',
    version='0.0.2',
    description='CLI interface to Clockify with the "MD - Innovatiekrediet" project',
    author='Gianluca, Ryan',
    packages=setuptools.find_packages(),
    install_requires=REQUIREMENTS,
    entry_points={
        'console_scripts':
            ['clockify = clockify_cli.clockify_main:entry_point']
    },
)